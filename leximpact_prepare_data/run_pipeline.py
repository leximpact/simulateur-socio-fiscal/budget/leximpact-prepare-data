from leximpact_common_python_libraries.config import Configuration
from leximpact_common_python_libraries.logger import logger as log

from leximpact_prepare_data.pipeline_survey_scenario import PipelineErfsSurveyScenario
from leximpact_prepare_data.pipeline_tax_and_benefit_system import pipeline_tbs
import os
import click

config = Configuration(project_folder="leximpact-prepare-data")
aggregates_path = config.get("AGREGATS_PATH")
log.debug("Le logger  leximpact_common_python_libraries.logger fonctionne !")
config_files_directory = os.path.join(
    config.project_module_path, ".config", "openfisca-survey-manager"
)


@click.command()
@click.option(
    "-erfs",
    "--annee_erfs",
    default=2019,
    help="ERFS-FPR year",
    show_default=True,
    type=int,
)
@click.option(
    "-pote", "--annee_pote", default=2022, help="POTE year", show_default=True, type=int
)
@click.option(
    "-calcul",
    "--annee_de_calcul",
    default=2022,
    help="POTE year",
    show_default=True,
    type=int,
)
def run_pipeline(annee_erfs, annee_pote, annee_de_calcul):
    log.debug("Create FranceTaxBenefitSystem")

    annee_pote = str(annee_pote)
    # Instanciation du scenario
    pipeline_survey_scenario = PipelineErfsSurveyScenario(
        config_files_directory=config_files_directory,
        annee_donnees=annee_erfs,
        period=annee_de_calcul,
        baseline_tax_benefit_system=pipeline_tbs,
        collection="openfisca_erfs_fpr",
        survey_name=f"openfisca_erfs_fpr_{annee_erfs}",
    )

    pipeline_survey_scenario.build_imputation(year=annee_pote)

    pipeline_survey_scenario.save_current_survey(
        variables=pipeline_survey_scenario.used_as_input_variables,
        collection="leximpact",
        survey_name=f"leximpact_{annee_de_calcul}",
        period=annee_de_calcul,
    )
