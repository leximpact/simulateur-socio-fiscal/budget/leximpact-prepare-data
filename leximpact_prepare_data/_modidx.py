# Autogenerated by nbdev

d = {
    "settings": {
        "branch": "master",
        "doc_baseurl": "/leximpact_prepare_data/",
        "doc_host": "https://documentation.leximpact.dev/",
        "git_url": "https://git.leximpact.dev/leximpact/simulateur-socio-fiscal/budget/leximpact-prepare-data/tree/master/",
        "lib_path": "leximpact_prepare_data",
    },
    "syms": {
        "leximpact_prepare_data.pipeline_survey_scenario": {},
        "leximpact_prepare_data.pipeline_tax_and_benefit_system": {},
        "leximpact_prepare_data.run_pipeline": {},
        "leximpact_prepare_data.scenario_tools.calib_and_copules": {},
        "leximpact_prepare_data.scenario_tools.comparison": {},
        "leximpact_prepare_data.scenario_tools.helpers_survey_scenario": {},
        "leximpact_prepare_data.scenario_tools.input_data_builder": {},
        "leximpact_prepare_data.scenario_tools.monte_carlo_computer": {},
    },
}
