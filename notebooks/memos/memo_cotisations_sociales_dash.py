"""
poetry run python notebooks/memos/memo_cotisations_sociales_dash.py

Sur le serveur :
pip install gunicorn
gunicorn -w 2 -b 0.0.0.0:5000 -t 100000 --max-requests 20 notebooks.memos.memo_cotisations_sociales_dash:server

http://localhost:5000/leximpact_prepare_data/memos/memo_cotisations_sociales_dash/

Cette dataviz en Dash est incluse dans le mémo, mais elle doit être 
exécutée à part pour être partagée : les mémos sont des fichiers HTML
 alors que Dash doit lancer un serveur, il faut donc le déployer côté
   serveur en complément du HTML.

"""

import pandas as pd
import numpy as np
import os
import plotly.graph_objects as go
import json
import copy
from openfisca_core.simulation_builder import SimulationBuilder
from leximpact_survey_scenario.leximpact_survey_scenario import leximpact_tbs

# from leximpact_common_python_libraries.config import Configuration
import dash
from dash import dcc, html, Output, Input


current_path = os.getcwd()
cotisations_type_cotisants = pd.DataFrame(
    {
        "Catégorie": [
            "Secteur privé",
            "Secteur public",
            "Travailleurs indépendants",
            "Salariés agricoles",
            "Exploitants agricoles",
            "Particuliers employeurs",
            "Autres actifs*",
            "Inactifs",
        ],
        "2022": [173.085, 68.560, 16.100, 5.941, 1.885, 3.129, 11.469, 0.907],
        "2023 (prévisions)": [
            183.353,
            71.468,
            16.177,
            6.215,
            2.064,
            3.356,
            11.715,
            0.960,
        ],
        "2024 (prévisions)": [
            191.079,
            73.725,
            16.864,
            6.391,
            2.187,
            3.486,
            11.738,
            1.006,
        ],
    }
)

cotisations_par_branche = {
    "branche": ["Maladie", " Vieillesse", "Famille", "AT-MP*", "Autonomie"],
    "cotisations_nettes": [88, 165.8, 36.4, 15.9, 0],
    "part_produits_nets": [
        round(100 * i, ndigits=2)
        for i in [88 / 242.7, 165.8 / 287.8, 36.4 / 58.8, 15.9 / 17.1, 0 / 41.2]
    ],
}

# config = Configuration(project_folder="leximpact-survey-scenario")

# %%
# parametres (pss, smic etc.)

# nombre d'heures temps plein
nb_heures_temps_plein_annuel = 1820

# pss
pss_annuel = 46368
pss_mensuel = pss_annuel / 12

# smic
smic_annuel = 21203
smic_mensuel = smic_annuel / 12
smic_horaire = smic_annuel / nb_heures_temps_plein_annuel

# %%
# niveaux de revenus pour les cas-types
x_min = 0
x_max = 10
x_pas = 0.05

revenu_range = np.arange(x_min, x_max + x_pas, x_pas)
revenu_range_length = len(revenu_range)
# print(revenu_range_length)

revenu_range = revenu_range * smic_annuel
# print(revenu_range)

# %%
# fonctions


def calculer_cotisations(
    path_cas_types, nom_cas_type, liste_variables, revenu, periode
):

    # importer le cas-type
    cas_types = os.path.join(path_cas_types, nom_cas_type)
    with open(cas_types) as f:
        temp = json.load(f)
        if temp.get("sliders"):
            del temp["sliders"]
    del temp["description"], temp["linked_variables"], temp["title"]

    # duplication du cas type en fonction du revenus
    list_cas_types = list()

    # boucle revenus
    for i in revenu_range:
        # print(i)
        temp2 = copy.deepcopy(temp)
        temp2["individus"]["Adulte 1"][revenu] = {
            "2023": i,
            "2024": i,
            "2025": i,
        }
        list_cas_types.append(temp2)
    data_frame = pd.DataFrame(list_cas_types)

    # cas-type
    cas_type = dict(
        data_frame.iloc[2][["familles", "foyers_fiscaux", "individus", "menages"]]
    )

    # pour chaque niveau de revenus on calcul le détail
    donnees = pd.DataFrame()
    for i in range(revenu_range_length):
        cas_type = dict(
            data_frame.iloc[i][["familles", "foyers_fiscaux", "individus", "menages"]]
        )
        simulation = SimulationBuilder()
        simulation = simulation.build_from_entities(leximpact_tbs, cas_type)
        indiv = dict()
        for variable in liste_variables:
            indiv[variable] = [simulation.calculate(variable, periode)[0]]
        donnees = pd.concat([donnees, pd.DataFrame(indiv)], axis=0)

        # cotisations en positif
        donnees = donnees.abs()

        # colonne smic
        donnees["smic"] = donnees.loc[:, revenu] / smic_mensuel

        # colonne pss
        donnees["pss"] = donnees.loc[:, revenu] / pss_mensuel

        # ordre des colonnes
        col = donnees.pop(revenu)
        donnees.insert(0, col.name, col)

        col = donnees.pop("pss")
        donnees.insert(0, col.name, col)

        col = donnees.pop("smic")
        donnees.insert(0, col.name, col)

    print(nom_cas_type)
    return donnees


def remplacer_valeurs(liste_donnees, colonne, valeurs):
    for i_donnees in np.arange(0, len(liste_donnees), 1):
        donnees = liste_donnees[i_donnees]
        for j in np.arange(0, len(valeurs), 1):
            donnees.loc[donnees[colonne] == j, colonne] = valeurs[j]


# %%
# chemin vers les cas-types
# path_cas_types = config.get("cas_type_chemin")

# %%
# liste des variables pour cas-types
liste_variables_cas_types = [
    # revenus
    "categorie_salarie",
    "salaire_de_base",
    "traitement_indiciaire_brut",
    "cotisations_employeur",
    # cotisations salariales (contributives)
    # prive
    "agirc_arrco_salarie",
    "apec_salarie",
    "contribution_equilibre_general_salarie",
    "contribution_equilibre_technique_salarie",
    "vieillesse_deplafonnee_salarie",
    "vieillesse_plafonnee_salarie",
    # public
    "ircantec_salarie",
    "pension_salarie",
    "rafp_salarie",
    # cotisations patronales (contributives)
    "ags",
    "agirc_arrco_employeur",
    "apec_employeur",
    "chomage_employeur",
    "contribution_equilibre_general_employeur",
    "contribution_equilibre_technique_employeur",
    "vieillesse_deplafonnee_employeur",
    "vieillesse_plafonnee_employeur",
    "fonds_emploi_hospitalier",
    "ircantec_employeur",
    "pension_employeur",
    "rafp_employeur",
    # cotisations patronales (non contributives)
    "ati_atiacl",
    "penibilite",
    "accident_du_travail",
    "contribution_solidarite_autonomie",
    "famille",
    "mmid_employeur",
    "taxe_salaires",
    "forfait_social",
    # autres
    "csg_deductible_salaire",
    "csg_imposable_salaire",
    "crds_salaire",
    "fnal_contribution",
    "versement_transport",
    "taxe_apprentissage",
    "contribution_formation_professionnelle",
    "financement_organisations_syndicales",
    "mmid_employeur_net_allegement",
    "famille_net_allegement",
    "smic_proratise",
    "allegement_general",
    "allegement_cotisation_maladie",
    "allegement_cotisation_maladie_base",
    "assiette_allegement",
    "allegement_cotisation_maladie_mode_recouvrement",
]

# %%
# | hide

# # calculer cotisations des cas-types
cas_type_prive_cadre = calculer_cotisations(
    "notebooks/memos/data",
    "ZZ_cotis_salarie_prive_cadre_3_SMIC.json",
    liste_variables_cas_types,
    "salaire_de_base",
    "2025-01",
)

# %%
# | hide

# # nettoyage des donnees : remplacer les valeur numériques par les labels correspondants

liste_donnees = [cas_type_prive_cadre]

# categorie_salarie
possible_values_categorie_salarie = [
    "prive_non_cadre",
    "prive_cadre",
    "public_titulaire_etat",
    "public_titulaire_militaire",
    "public_titulaire_territoriale",
    "public_titulaire_hospitaliere",
    "public_non_titulaire",
    "non_pertinent",
]

remplacer_valeurs(liste_donnees, "categorie_salarie", possible_values_categorie_salarie)

# %%
# config-openfisca-survey-manager
cwd = os.getcwd()
# config_dir = os.path.join(
#     os.path.abspath(os.path.join(cwd, os.pardir)), ".config/openfisca-survey-manager"
# )

# %%
# copie de la dataframe du cas-type pour le graphique
data_graph_dash = copy.deepcopy(cas_type_prive_cadre)

# %%
# filter une partie des données (smic)
data_graph_dash = data_graph_dash.loc[data_graph_dash["smic"] <= 10]

# %%
# attention - ajustement du taux de la cotisation atmp (uniquement la part mutualisée dans l'allègement)
data_graph_dash["atmp_mutualisee"] = 0.0046 * data_graph_dash["salaire_de_base"]

# %%
# selectionner les colonnes
data_graph_dash = data_graph_dash[
    [
        "smic",
        "salaire_de_base",
        "allegement_general",
        # employeur
        "mmid_employeur",
        "mmid_employeur_net_allegement",
        "vieillesse_deplafonnee_employeur",
        "vieillesse_plafonnee_employeur",
        "famille",
        "famille_net_allegement",
        "atmp_mutualisee",
        "contribution_solidarite_autonomie",
        "agirc_arrco_employeur",
        "contribution_equilibre_general_employeur",
        "chomage_employeur",
        "ags",
        "fnal_contribution",
        "apec_employeur",
        "contribution_equilibre_technique_employeur",
        # salarie
        "vieillesse_deplafonnee_salarie",
        "vieillesse_plafonnee_salarie",
        "agirc_arrco_salarie",
        "contribution_equilibre_general_salarie",
        "contribution_equilibre_technique_salarie",
        "apec_salarie",
    ]
]


# %%
def calculate_taux_allegement_general(assiette):
    # Calculer le ratio
    ratio = 0.3194 / 0.6 * ((1.6 * smic_mensuel / assiette) - 1)
    ratio_constrained = np.maximum(np.minimum(ratio, 0.3194), 0)
    return ratio_constrained


assiette_allegement_general = [
    "mmid_employeur",
    "vieillesse_deplafonnee_employeur",
    "vieillesse_plafonnee_employeur",
    "famille",
    "atmp_mutualisee",
    "contribution_solidarite_autonomie",
    "agirc_arrco_employeur",
    "contribution_equilibre_general_employeur",
    "chomage_employeur",
    "fnal_contribution",
]

# %%
# # graph dash cotisations
# initialisation de l'application Dash

# app1 = dash.Dash(__name__, sharing=True, csrf_protect=False, server=server)
app1 = dash.Dash(
    __name__,
    url_base_pathname="/leximpact_prepare_data/memos/memo_cotisations_sociales_dash/",
)

server = app1.server
# layout de l'app
app1.layout = html.Div(
    [
        html.H2("Cotisations (salarié du privé cadre)"),  # titre
        # Conteneur pour les boutons employeur-salarie et taux-montants
        html.Div(
            [
                html.Div(
                    [
                        # bouton employeur-salarie
                        dcc.Dropdown(
                            id="employeur-salarie",
                            options=[
                                {"label": "Employeur", "value": "Employeur"},
                                {"label": "Salarié", "value": "Salarié"},
                            ],
                            value="Employeur",
                            style={
                                "width": "300px",
                                "fontFamily": "Arial, sans-serif",
                                "color": "#333333",
                            },
                            clearable=False,
                        ),
                        # bouton taux-montants
                        dcc.Dropdown(
                            id="taux-montants",
                            options=[
                                {"label": "Montants", "value": "Montants"},
                                {"label": "Taux", "value": "Taux"},
                            ],
                            value="Montants",
                            style={
                                "width": "300px",
                                "fontFamily": "Arial, sans-serif",
                                "color": "#333333",
                            },
                            clearable=False,
                        ),
                    ],
                    style={"display": "inline-block", "vertical-align": "top"},
                ),
                # Conteneur pour les boutons des allègements
                html.Div(
                    [
                        # bouton allègement maladie
                        dcc.Checklist(
                            id="activer-allegement-maladie",
                            options=[
                                {
                                    "label": "Allègement maladie",
                                    "value": "activer_allegement_maladie",
                                }
                            ],
                            value=["activer_allegement_maladie"],
                            style={
                                "width": "300px",
                                "fontFamily": "Arial, sans-serif",
                                "color": "#333333",
                            },
                        ),
                        # bouton allègement famille
                        dcc.Checklist(
                            id="activer-allegement-famille",
                            options=[
                                {
                                    "label": "Allègement famille",
                                    "value": "activer_allegement_famille",
                                }
                            ],
                            value=["activer_allegement_famille"],
                            style={
                                "width": "300px",
                                "fontFamily": "Arial, sans-serif",
                                "color": "#333333",
                            },
                        ),
                        # bouton allègement general
                        dcc.Checklist(
                            id="activer-allegement-general",
                            options=[
                                {
                                    "label": "Allègement général",
                                    "value": "activer_allegement_general",
                                }
                            ],
                            value=["activer_allegement_general"],
                            style={
                                "width": "300px",
                                "fontFamily": "Arial, sans-serif",
                                "color": "#333333",
                            },
                        ),
                    ],
                    style={
                        "display": "inline-block",
                        "vertical-align": "top",
                        "margin-left": "20px",
                    },
                ),
            ]
        ),
        dcc.Graph(id="graph"),
        html.Div(id="debug-output"),
        dcc.Slider(
            id="xaxis-slider",
            min=1,
            max=10,
            step=1,
            value=4,  # Valeur initiale
            marks={i: f"{i} Smic" for i in range(1, 11)},
        ),
    ],
    style={
        "backgroundColor": "white",
        # 'border': '1px solid lightgrey',  # Style pour la bordure
        "padding": "0px",
        "fontFamily": "Segoe UI",
        "width": "80%",  # Ajuster la largeur de l'app
        "height": "80vh",  # Ajuster la hauteur de l'app
        # "margin": "auto",  # Centrer l'app
    },
)

# Callback pour mettre à jour le graphique


@app1.callback(
    Output("graph", "figure"),
    Output("debug-output", "children"),
    Input("employeur-salarie", "value"),
    Input("taux-montants", "value"),
    Input("activer-allegement-maladie", "value"),
    Input("activer-allegement-famille", "value"),
    Input("activer-allegement-general", "value"),
    Input("xaxis-slider", "value"),
)
def update_graph(
    employeur_salarie,
    taux_montants,
    activer_allegement_maladie,
    activer_allegement_famille,
    activer_allegement_general,
    xaxis_max,
):
    # Employeur / Montants
    if employeur_salarie == "Employeur" and taux_montants == "Montants":
        df = copy.deepcopy(data_graph_dash)  # copie de la dataframe
        df = df.loc[df["smic"] <= xaxis_max].reset_index(drop=True)
        # selectionner les colonnes utiles dans df
        df = df[
            [
                "salaire_de_base",
                "mmid_employeur",
                "vieillesse_deplafonnee_employeur",
                "vieillesse_plafonnee_employeur",
                "famille",
                "atmp_mutualisee",
                "contribution_solidarite_autonomie",
                "agirc_arrco_employeur",
                "contribution_equilibre_general_employeur",
                "chomage_employeur",
                "ags",
                "fnal_contribution",
                "apec_employeur",
                "contribution_equilibre_technique_employeur",
            ]
        ]

        df["total"] = df.sum(axis=1) - df["salaire_de_base"]

        if activer_allegement_maladie:
            df_bis = copy.deepcopy(
                data_graph_dash
            )  # df bis pour garder une copie non modifiee de data_graph_dash
            df_bis = df_bis.loc[df_bis["smic"] <= xaxis_max].reset_index(
                drop=True
            )  # selectionner une partie de la df_bis (slider)
            df.loc[:, "mmid_employeur"] = df_bis["mmid_employeur_net_allegement"]
            # df = df.rename(columns={'mmid_employeur': 'mmid_employeur_net_allegement'})

        if activer_allegement_famille:
            df_bis = copy.deepcopy(
                data_graph_dash
            )  # df bis pour garder une copie non modifiee de data_graph_dash
            df_bis = df_bis.loc[df_bis["smic"] <= xaxis_max].reset_index(
                drop=True
            )  # selectionner une partie de la df_bis (slider)
            df.loc[:, "famille"] = df_bis["famille_net_allegement"]
            # df = df.rename(columns={'famille': 'famille_net_allegement'})

        # df.loc[:, "total_allegement_general"] = (df.sum(axis=1) - df["salaire_de_base"] - df["contribution_equilibre_technique_employeur"])
        if activer_allegement_general:
            df["assiette_allegement_general"] = df[assiette_allegement_general].sum(
                axis=1
            )
            df["taux_allegement_general"] = calculate_taux_allegement_general(
                df["salaire_de_base"]
            )
            for column in assiette_allegement_general:
                df[column] = np.maximum(
                    df[column] - df[column] * df["taux_allegement_general"] / 0.3194, 0
                )

            df = df.drop("assiette_allegement_general", axis=1)
            df = df.drop("taux_allegement_general", axis=1)

        df.loc[:, "total_net_allegements"] = (
            df.sum(axis=1) - df["salaire_de_base"] - df["total"]
        )

        df = df.rename(
            columns={
                "mmid_employeur": "MMID",
                # 'mmid_employeur_net_allegement': 'MMID (allèg.)',
                "vieillesse_deplafonnee_employeur": "Vieillesse déplafonnée",
                "vieillesse_plafonnee_employeur": "Vieillesse plafonnée",
                "famille": "Famille",
                # 'famille_net_allegement': 'Famille (allèg.)',
                "atmp_mutualisee": "ATMP",
                "contribution_solidarite_autonomie": "CSA",
                "agirc_arrco_employeur": "Agirc-Arrco",
                "contribution_equilibre_general_employeur": "CEG",
                "chomage_employeur": "Chômage",
                "ags": "AGS",
                "fnal_contribution": "FNAL",
                "apec_employeur": "APEC",
                "contribution_equilibre_technique_employeur": "CET",
            }
        )
        suffix = " €"

    # Employeur / Taux
    elif employeur_salarie == "Employeur" and taux_montants == "Taux":
        df = copy.deepcopy(data_graph_dash)  # copie de la dataframe
        df = df.loc[df["smic"] <= xaxis_max].reset_index(drop=True)
        df = df[
            [
                "salaire_de_base",
                "mmid_employeur",
                "vieillesse_deplafonnee_employeur",
                "vieillesse_plafonnee_employeur",
                "famille",
                "atmp_mutualisee",
                "contribution_solidarite_autonomie",
                "agirc_arrco_employeur",
                "contribution_equilibre_general_employeur",
                "chomage_employeur",
                "ags",
                "fnal_contribution",
                "apec_employeur",
                "contribution_equilibre_technique_employeur",
            ]
        ]

        df.loc[:, "total"] = df.sum(axis=1) - df.loc[:, "salaire_de_base"]

        if activer_allegement_maladie:
            df_bis = copy.deepcopy(
                data_graph_dash
            )  # df bis pour garder une copie non modifiee de data_graph_dash
            df_bis = df_bis.loc[df_bis["smic"] <= xaxis_max].reset_index(
                drop=True
            )  # selectionner une partie de la df_bis (slider)
            df.loc[:, "mmid_employeur"] = df_bis["mmid_employeur_net_allegement"]
            # df = df.rename(columns={'mmid_employeur': 'mmid_employeur_net_allegement'})

        if activer_allegement_famille:
            df_bis = copy.deepcopy(
                data_graph_dash
            )  # df bis pour garder une copie non modifiee de data_graph_dash
            df_bis = df_bis.loc[df_bis["smic"] <= xaxis_max].reset_index(
                drop=True
            )  # selectionner une partie de la df_bis (slider)
            df.loc[:, "famille"] = df_bis["famille_net_allegement"]
            # df = df.rename(columns={'famille': 'famille_net_allegement'})

        if activer_allegement_general:
            df["assiette_allegement_general"] = df[assiette_allegement_general].sum(
                axis=1
            )
            df["taux_allegement_general"] = calculate_taux_allegement_general(
                df["salaire_de_base"]
            )
            for column in assiette_allegement_general:
                df[column] = np.maximum(
                    df[column] - df[column] * df["taux_allegement_general"] / 0.3194, 0
                )
            df = df.drop("assiette_allegement_general", axis=1)
            df = df.drop("taux_allegement_general", axis=1)
            df = df.clip(lower=0)

        # df.loc[:, "total_allegement_general"] = (df.sum(axis=1) - df["salaire_de_base"] - df["contribution_equilibre_technique_employeur"])
        # if "activer_allegement_general" in activer_allegement_general:
        #     for column in df.columns:
        #         if column not in ["salaire_de_base", "total"]:
        #             df[column] = df[column] - (df[column] / (df['total'] - df['contribution_equilibre_technique_employeur'])) * data_graph_dash['allegement_general']
        # df = df.drop('total_allegement_general', axis=1)

        for column in df.columns:
            if column not in ["salaire_de_base"]:
                df.loc[:, column] = round(df[column] / df["salaire_de_base"] * 100, 2)

        df.loc[:, "total_net_allegements"] = (
            df.sum(axis=1) - df.loc[:, "salaire_de_base"] - df.loc[:, "total"]
        )

        df = df.rename(
            columns={
                "mmid_employeur": "MMID",
                # 'mmid_employeur_net_allegement': 'MMID (allèg.)',
                "vieillesse_deplafonnee_employeur": "Vieillesse déplafonnée",
                "vieillesse_plafonnee_employeur": "Vieillesse plafonnée",
                "famille": "Famille",
                # 'famille_net_allegement': 'Famille (allèg.)',
                "atmp_mutualisee": "ATMP",
                "contribution_solidarite_autonomie": "CSA",
                "agirc_arrco_employeur": "Agirc-Arrco",
                "contribution_equilibre_general_employeur": "CEG",
                "chomage_employeur": "Chômage",
                "ags": "AGS",
                "fnal_contribution": "FNAL",
                "apec_employeur": "APEC",
                "contribution_equilibre_technique_employeur": "CET",
            }
        )
        suffix = " %"

    # Salarie / Montants
    elif employeur_salarie == "Salarié" and taux_montants == "Montants":
        df = copy.deepcopy(data_graph_dash)  # copie de la dataframe
        df = df.loc[df["smic"] <= xaxis_max].reset_index(drop=True)
        df = df[
            [
                "salaire_de_base",
                "vieillesse_deplafonnee_salarie",
                "vieillesse_plafonnee_salarie",
                "agirc_arrco_salarie",
                "contribution_equilibre_general_salarie",
                "contribution_equilibre_technique_salarie",
                "apec_salarie",
            ]
        ]
        df.loc[:, "total"] = df.sum(axis=1) - df["salaire_de_base"]
        df.loc[:, "total_net_allegements"] = df["total"]

        df = df.rename(
            columns={
                "vieillesse_deplafonnee_salarie": "Vieillesse déplafonnée",
                "vieillesse_plafonnee_salarie": "Vieillesse plafonnée",
                "agirc_arrco_salarie": "Agirc-Arrco",
                "contribution_equilibre_general_salarie": "CEG",
                "contribution_equilibre_technique_salarie": "CET",
                "apec_salarie": "APEC",
            }
        )
        suffix = " €"

    # Salarie / Taux
    elif employeur_salarie == "Salarié" and taux_montants == "Taux":
        df = copy.deepcopy(data_graph_dash)
        df = df[
            [
                "salaire_de_base",
                "vieillesse_deplafonnee_salarie",
                "vieillesse_plafonnee_salarie",
                "agirc_arrco_salarie",
                "contribution_equilibre_general_salarie",
                "contribution_equilibre_technique_salarie",
                "apec_salarie",
            ]
        ]
        for column in df.columns:
            if column not in ["salaire_de_base"]:
                df.loc[:, column] = round(df[column] / df["salaire_de_base"] * 100, 2)
        df.loc[:, "total"] = df.sum(axis=1) - df["salaire_de_base"]
        df.loc[:, "total_net_allegements"] = df["total"]

        df = df.rename(
            columns={
                "mmid_employeur": "MMID",
                "vieillesse_deplafonnee_salarie": "Vieillesse déplafonnée",
                "vieillesse_plafonnee_salarie": "Vieillesse plafonnée",
                "agirc_arrco_salarie": "Agirc-Arrco",
                "contribution_equilibre_general_salarie": "CEG",
                "contribution_equilibre_technique_salarie": "CET",
                "apec_salarie": "APEC",
            }
        )
        suffix = " %"

    debug_message = ""  # debug message (pour vérifications des boutons sélectionnés)

    fig = go.Figure()  # initialiser le graphique

    # afficher les colonnes cotisations
    for column in df.columns:
        if column not in ["salaire_de_base", "total", "total_net_allegements"]:
            fig.add_trace(
                go.Scatter(
                    x=df["salaire_de_base"],
                    y=df[column],
                    mode="lines",
                    stackgroup="one",
                    name=column,
                )
            )
    fig.add_trace(
        go.Scatter(
            x=df["salaire_de_base"],
            y=df["total_net_allegements"],
            mode="lines",
            name="Total net d'allègements",
            line=dict(color="#C00000", dash="longdashdot", width=3),
        )
    )
    fig.add_trace(
        go.Scatter(
            x=df["salaire_de_base"],
            y=df["total"],
            mode="lines",
            name="Total brut d'allègements",
            line=dict(color="black", dash="dash", width=3),
        )
    )
    fig.update_layout(
        title="",
        xaxis=dict(
            title="Salaire brut",
            range=[smic_mensuel, xaxis_max * smic_mensuel],
            ticksuffix=" €",
            gridcolor="#F2F2F2",
            linecolor="#F2F2F2",
        ),
        yaxis=dict(
            title="",
            # range=[0, None],
            rangemode="tozero",
            ticksuffix=suffix,
            gridcolor="#F2F2F2",
            linecolor="#F2F2F2",
        ),
        margin=dict(l=5, r=5, t=30, b=30),
        height=350,  # hauteur du graphique
        width=700,  # largeur du graphique
        plot_bgcolor="white",  # Couleur de fond du graphique
        paper_bgcolor="white",  # Couleur de fond de l'ensemble de la figure
    )
    # lignes verticales smic
    for hline_smic in [1, 1.6, 2.5, 3.5]:
        fig.add_vline(
            x=hline_smic * smic_mensuel,
            line_dash="dot",
            line_width=1,
            line_color="grey",
            annotation_text=f"{hline_smic} Smic",
            annotation_position="top",
        )
    # lignes verticales pss
    for hline_pss in range(1, 9):
        fig.add_vline(
            x=hline_pss * pss_mensuel,
            line_dash="dot",
            line_width=1,
            line_color="#A6A6A6",
            annotation_text=f"{hline_pss} PSS",
            annotation_position="top",
            annotation_font_color="#A6A6A6",
        )
    return fig, debug_message


# Exécution de l'application
if __name__ == "__main__":
    app1.run_server(debug=False)
