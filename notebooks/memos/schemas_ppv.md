

# PEPA
```mermaid
flowchart TD
    A[Prime PEPA] --> B{Accord interessement ?}
    B -- Oui --> C[Plafond = 2000]
    C --> F
    B -- Non --> J{Effectif < 50 ?}
    J -- Oui --> C
    J -- Non --> E[Plafond = 1000]
    E --> F{Salaire < 3 SMIC}
    F -- Oui --> G{Date < 31 mars 2022 ?}
    F -- Non --> I[Aucune Exoneration]
    G -- Oui --> H[Exoneration totale sous plafond]
    G -- Non --> I

```

# PPV Ancienne version codée
```mermaid
flowchart TD
    A[Prime PPV] --> B{Accord interessement ?}
    B -- Oui --> C[Plafond = 6000]
    C --> F
    B -- Non --> E[Plafond = 3000]
    E --> F{Salaire < 3 SMIC}
    F -- Oui --> G{Date < 1er janvier 2024 ?}
    F -- Non --> I[Exoneration partielle sous plafond]
    G -- Oui --> H[Exoneration totale sous plafond]
    G -- Non --> I
    I --> K[CSG]
    K --> L[CRDS]
    L --> N[Impôt sur le revenu]
    N --> M[Forfait social]
    M --> Z[PPV intégrée au RFR]
    H --> Z
```

# PPV Nouvelle version
```mermaid
flowchart TD
    A[Prime PPV] --> B{Accord interessement ?}
    B -- Oui --> C[Plafond = 6000]
    C --> F
    B -- Non --> E[Plafond = 3000]
    E --> F{Date >= 1er janvier 2024 ?}
    F -- Oui : Prime pérenne --> I
    F -- Non --> G{Choix global de l'entreprise\n pour la prime pérenne ?}
    I[Exoneration partielle sous plafond]
    H[Exoneration totale sous plafond]
    G -- Non : Prime temporaire --> H
    G -- Oui : Prime pérenne --> I
    I --> K[CSG]
    K --> L[CRDS]
    L --> N[Impôt sur le revenu]
    N --> M[Forfait social]
    M --> Z[PPV intégrée au RFR]
    H --> Z
```
# PPV exceptionnelle implémentée

```mermaid
flowchart TD
    A[PPV exceptionnelle] --> prime_partage_valeur_exceptionnelle
    prime_partage_valeur_exceptionnelle --> prime_partage_valeur_exoneree_exceptionnelle
    accord_interessement --> prime_partage_valeur_exoneree_exceptionnelle
    plafond_exoneration_avec_accord_interessement --> prime_partage_valeur_exoneree_exceptionnelle
    plafond_exoneration --> prime_partage_valeur_exoneree_exceptionnelle
    prime_partage_valeur_exoneree_exceptionnelle --> prime_partage_valeur_non_exoneree_exceptionnelle
    salaire_de_base --> ppv_eligibilite_exceptionnelle
    smic_proratise --> ppv_eligibilite_exceptionnelle
    quotite_de_travail --> ppv_eligibilite_exceptionnelle
    ppv_eligibilite_exceptionnelle --> prime_partage_valeur_exoneree_exceptionnelle
    ppv_eligibilite_exceptionnelle --> prime_partage_valeur_non_exoneree_exceptionnelle
    prime_partage_valeur_exoneree_exceptionnelle --> salaire_super_brut
    prime_partage_valeur_exoneree_exceptionnelle --> rfr
    prime_partage_valeur_non_exoneree_exceptionnelle --> primes_salaires_non_exonerees
    primes_salaires_non_exonerees --> assiette_csg_abattue
    prime_partage_valeur_non_exoneree_exceptionnelle --> assiette_csg_abattue
    prime_partage_valeur_non_exoneree_exceptionnelle --> salaire_imposable
    assiette_csg_abattue --> crds_salaire
    assiette_csg_abattue --> csg_imposable_salaire
    assiette_csg_abattue --> csg_deductible_salaire
    crds_salaire --> salaire_net
    csg_imposable_salaire --> salaire_net
    prime_partage_valeur_exoneree_exceptionnelle --> salaire_net
    csg_deductible_salaire --> salaire_imposable
    salaire_imposable --> rfr
    salaire_imposable --> irpp
```

# PPV pérenne implémentation

```mermaid
flowchart TD
    A[PPV pérenne] --> prime_partage_valeur
    prime_partage_valeur --> prime_partage_valeur_exoneree
    accord_interessement --> prime_partage_valeur_exoneree
    plafond_exoneration_avec_accord_interessement --> prime_partage_valeur_exoneree
    plafond_exoneration --> prime_partage_valeur_exoneree
    prime_partage_valeur_exoneree --> prime_partage_valeur_non_exoneree
    prime_partage_valeur --> prime_partage_valeur_non_exoneree
    primes_salaires_non_exonerees --> assiette_csg_abattue
    assiette_csg_abattue --> crds_salaire
    assiette_csg_abattue --> csg_imposable_salaire
    assiette_csg_abattue --> csg_deductible_salaire
    prime_partage_valeur_exoneree --> salaire_super_brut
    prime_partage_valeur_exoneree --> salaire_net
    salaire_imposable --> salaire_net
    crds_salaire --> salaire_net
    csg_imposable_salaire --> salaire_net
    prime_partage_valeur_non_exoneree --> primes_salaires_non_exonerees
    prime_partage_valeur_non_exoneree --> salaire_imposable
    csg_deductible_salaire --> salaire_imposable
    prime_partage_valeur --> forfait_social
    prime_partage_valeur_exoneree --> rfr
    salaire_imposable --> rfr
    salaire_imposable --> irpp
```

# PPV

## Forfait social au titre de l’intéressement
La prime de partage de la valeur est assimilée, pour l’assujettissement à
la contribution prévue à l’article L. 137-15 du code de la sécurité sociale, aux
sommes versées au titre de l’intéressement mentionné au titre Ier du livre III
de la troisième partie du code du travail.

- Exo partielle :

# Forfait social
- taux_reduit_1 8%: financement des prestations complémentaires de prévoyance versées au bénéfice de leurs salariés, anciens salariés et de leurs ayants droit ; la réserve spéciale de participation dans les sociétés coopératives de production (Scop) soumises à la particpiation.
- taux_reduit_2 16%: versements alimentant un plan d’épargne pour la retraite collectif
- taux_reduit_3 10%: Depuis le 1er janvier 2019, dans les entreprises employant au moins 50 salariés, l’abondement de l’employeur sur la contribution des salariés à l’acquisition de titres de l’entreprise ou d’une entreprise liée est soumis au forfait social de 10 % (contre 20 % antérieurement).
- taux_plein 20%:

# Tests PPV
