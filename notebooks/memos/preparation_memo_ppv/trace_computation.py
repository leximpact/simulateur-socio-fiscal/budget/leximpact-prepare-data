"""
poetry run python -m openfisca_france_reforms.prime_partage_valeur.scripts.trace_computation > debug.log
"""

import sys

import numpy as np

# from openfisca_france import FranceTaxBenefitSystem
from openfisca_core.simulation_builder import SimulationBuilder
from openfisca_france_with_indirect_taxation import CountryTaxBenefitSystem

from .. import PrimePartageValeur


def main():
    period = "year:2021:3"
    NB_INDIVIDUS = 2
    tax_benefit_system = CountryTaxBenefitSystem()
    reform = PrimePartageValeur(tax_benefit_system)
    sb = SimulationBuilder()
    simulation = sb.build_default_simulation(reform, count=NB_INDIVIDUS)
    # activate the trace
    simulation.trace = True
    # Ajoute les salaires
    salaire_de_base = [26_070 * 3, 26_070 * 3]
    simulation.set_input(
        "salaire_de_base",
        period,
        np.array(salaire_de_base),
    )
    primes_salaires = [1_000, 0]
    simulation.set_input(
        "primes_salaires",
        "2023-09",
        np.array(primes_salaires),
    )
    # prime_partage_valeur_exceptionnelle = [0,0]
    # simulation.set_input(
    #     "prime_partage_valeur_exceptionnelle",
    #     period,
    #     np.array(prime_partage_valeur_exceptionnelle),
    # )
    prime_partage_valeur = [0, 1_000]
    simulation.set_input("prime_partage_valeur", "2023", np.array(prime_partage_valeur))
    simulation.set_input("effectif_entreprise", period, np.array([250, 250]))
    simulation.set_input("accord_interessement", period, np.array([False, False]))
    # # Ajoute les salaires
    # salaire_de_base = [2_000 * 12, 2_000 * 12, 2_000 * 12, 2_000 * 12, 2_000 * 12]
    # simulation.set_input(
    #     "salaire_de_base",
    #     period,
    #     np.array(salaire_de_base),
    # )
    # primes_salaires = [3_000, 0, 0, 0, 0]
    # simulation.set_input(
    #     "primes_salaires",
    #     period,
    #     np.array(primes_salaires),
    # )
    # prime_partage_valeur_exceptionnelle = [0, 0, 0, 0, 0]
    # simulation.set_input(
    #     "prime_partage_valeur_exceptionnelle",
    #     period,
    #     np.array(prime_partage_valeur_exceptionnelle),
    # )
    # prime_partage_valeur = [0, 0, 0, 3_000, 4_200]
    # simulation.set_input("prime_partage_valeur", period, np.array(prime_partage_valeur))
    # simulation.set_input("effectif_entreprise", period, np.array([65, 65, 65, 65, 65]))
    # simulation.set_input(
    #     "accord_interessement", period, np.array([False, False, False, False, False])
    # )

    # Calcul le salaire_super_brut
    period = "2023"
    salaire_super_brut = simulation.calculate_add("salaire_super_brut", period)
    salaire_net = simulation.calculate_add("salaire_net", period)
    revenu_disponible = simulation.calculate_add("revenu_disponible", period)
    irpp = simulation.calculate_add("irpp", period)
    for i, salaire in enumerate(salaire_super_brut):
        print(
            f"Individus {i} salaire_de_base {salaire_de_base[i]} €. primes_salaires={primes_salaires[i]}€ PPVP={prime_partage_valeur[i]} €"
        )
        print(
            f"   salaire_super_brut {salaire} €. salaire_net={salaire_net[i]}€ L'IRPP est {irpp[i]} € => {revenu_disponible[i]=} € pour 2023"
        )
    # print calculation steps
    simulation.tracer.print_computation_log()


if __name__ == "__main__":
    sys.exit(main())
