# Documentation du code de branchement de la base de données POTE à Openfisca

Ce document explique les différentes étapes qui permettent, à partir de la base de données POTE complète, de réaliser une simulation budgétaire de l'impôt sur le revenu avec OpenFisca France. La base de données POTE est disponible sur demande auprès des producteurs de données via le CASD. ***Pour le moment le code est également sur le CASD uniquement.***

## Introduction : quel est le champ de POTE et quelles différences les données du simulateur en ligne ?

#### Les individus présents dans POTE

POTE est une base de données administrative mise à disposition par la Direction générale des finances publiques (DGFIP). Elle contient l'exhaustivité des déclarations d'impôt sur le revenu françaises. Les personnes qui sont concernées par cette déclaration sont l'ensemble des personnes domiciliées fiscalement en France ou non résidentes fiscales mais percevant des revenus de sources françaises. Toutefois dans certains cas une convention internationale peut prévoir des règles fiscales différentes. Les déclarations comportent les informations de l'ensemble des personnes du foyer fiscal. 

#### La différence avec les individus présents dans l'ERFS

l'ERFS est une enquête de l'INSEE sur 50 000 ménages que nous utilisons pour les calculs budgétaires de notre simulateur en ligne. Voici les principales différences de champ entre les deux bases :

- l'ERFS est un échantillon pondéré, représentatif de la population mais non exhaustif. De ce fait la base présente moins de précision que POTE, par exemple sur les très hauts revenus.

- l'ERFS concerne les ménages ordinaires vivant en France métropolitaine dont la personne de référence n'est pas étudiante. Certains individus sont donc présents dans POTE mais pas dans l'ERFS :
    - Les personnes ne vivant pas en ménage ordinaire, par exemple les résidants en maison de retraite médicalisées
    - Les personnes résidant dans les DROM-COM
    - Les personnes vivant à l'étranger avec des revenus de source française
    - Les ménages dont la personne de référence est étudiante

#### Une différence d'échelle : le foyer fiscal vs le ménage

Dans POTE nous observons qu'une seule entité, les foyers fiscaux, une entité spécifique à la déclaration fiscale. A l'inverse dans l'ERFS nous observons plusieurs entités utilisées dans différentes parties du système socio-fiscal : les ménages, ensemble des individus vivant ensembles, les familles au sans de la caisse nationale des allocations familiales (CNAF) et les foyers fiscaux. Ainsi par exemple un couple en union libre sera dans le même ménage et dans la même famille s'il cohabite mais dans deux foyers fiscaux séparés. Si dans l'ERFS nous pouvons observer leur situation dans son ensemble, dans POTE nous n'avons que les deux foyers fiscaux séparés et nous ne savons pas qu'il s'agit d'un couple en union libre. Dans cette base de données les deux membres du couple aparaissent comme des célibataire seul dans leur foyer fiscal.

Cette différence implique que le champs de ce que nous pouvons calculer avec POTE est plus restreint, puisque par exemple nous n'avons pas l'ensemble des informations nécessaires pour calculer les prestations sociales.

Cela implique aussi une différence dans la représentation des simulations réalisées. Usuellement lorsque l'ont regarde le système socio-fiscal dans sont ensemble et la redistribution qui y est opérée on regarde le niveau de vie des individus à l'échelle de leur ménage. Cette notion suppose qu'il y a du partage de revenu entre les personnes d'un même ménage et des économies du fait de la cohabitation. Avec POTE nous ne pouvons pas observer le ménage ni calculer le revenu disponible, les résultats ne sont donc pas tout à fait comparables entre les deux tables.

## Etape préliminaire : passage de la base POTE d'un format SAS à parquet

***A améliorer/optimiser cette étape est la première étape d'investigation du format parquet sur le CASD, avec une meilleure connaissance de ce format, de pyarrow et pyspark il y a sûrement des améliorations à faire***

La base de données POTE brute est en format de données SAS. Pour faciliter la manipulation de cette base très volumineuse cette étape préliminaire vise à changer le format de la base sans retraitement de son contenu, afin de la conserver la plus brute possible. L'opération est réalisée en 2 étapes :

- Lecture de la base par morceaux (chunks) par contrainte de mémoire vive, et écriture de ces chunks en .parquet. (XXTODOXX mettre le lien vers le script correspondant)

- Lecture des chunks parquets pour réécrire un fichier `.parquet` par colonne. Le format de fichier `.parquet` optimise la mémoire par colonne, il semblait donc plus pertinant de travailler à partir de fichiers de colonnes.

Dans les étapes suivantes, nous appelons *POTE brut* les données issues de cette étape, c'est-à-dire l'ensemble des fichiers de colonnes de POTE en format parquet.

    --> Idée d'amélioration : il semblerait qu'il existe une méthode de Spark pour lire du parquet, cela permettrait peut être de faire cette étape bien plus vite. Pour l'instant cela prend plusieurs jours.

    --> Normalement le format SAS doit disparaître de l'administration donc nous aurons peut être directement les bases au format parquet à l'avenir.


## Création d'un tax_and_benefit_system spécifique à POTE

Certaines variables de revenus d'Openfisca France sont définies à l'échelle mensuelle pour le besoin du calcul des cotisations et des minimas sociaux. Or pour le calcul de l'impôt sur le revenu nous disposons de données annuelles et réalisons un calcul annuel. Pour alléger le modèle et les calculs le [tax_and_benefit_system](https://openfisca.org/doc/key-concepts/tax_and_benefit_system.html) utilisé est une réforme du [système socio fiscal définit dans Openfisca France](https://github.com/openfisca/openfisca-france) où certaines variables de revenus ont été annualisées. Il s'agit des variables:

- salaire_imposable
- retraite_imposable
- chômage_imposable
- revenus_capitaux_prelevement_forfaitaire_unique_ir
- revenus_capitaux_prelevement_bareme
- revenus_capitaux_prelevement_liberatoire


De plus, nous ajoutons dans notre tax_and_benefit_system des variables spécifiques pour [l'imputation de revenus](https://documentation.leximpact.dev/leximpact_prepare_data/index.html) dans la base ERFS-FPR :

- revenus_individuels : il s'agit de la somme des revenus individuels au niveau de foyer fiscal.
- salaire_imposable_base_large : Il s'agit de l'ensemble des cases fiscales de la catégorie `traitement et salaire` sans le chômage.
- rfr_par_part


## Création d'un Survey scenario spécifique à POTE

Création d'une classe `PoteSurveyScenario` héritée de la classe `AbstractSurveyScenario` de [openfisca-survey-manager](https://github.com/openfisca/openfisca-survey-manager).

Un survey scenario est un objet python qui permet de brancher une base de données contenant des informations sur les ménages / individus à un tax_and_benefit system openfisca afin de calculer les dispositif du système socio fiscal en fonction des informations de la base de données.

Le survey scenario que nous construisons dans cette étape a comme tax_benefit_system celui définit dans la section précédente.

Les modifications sont minimes par rapport à la classe mère. Il s'agit surtout de spécifier les input variables utilisées. Cela est fait de manière automatique en prenant les cases fiscales retrouvées à la fois dans Openfisca France et dans POTE. En effet certaines cases fiscales très spécifiques ne sont pour le moment pas codées dans le modèle.

L'autre ajout dans cette classe est la définition d'une méthode `custom_input_data_frame` qui redéfinie certaines cases fiscales d'entrée, notamment concernant les cases donnant droit à des demis parts complémentaires. Le choix de mettre ces opérations dans cette étape et non dans la préparation des tables (voir sections suivantes) tient du fait qu'elles sont faites avec pandas.

    --> Amélioration : faire en sorte que le `custom_input_data_frame` fonctionne même si les colonnes ne sont pas trouvées dans les tables d'entrée. Pour l'instant et de manière temporaire il y a une option dans la classe pour réaliser ou non cette étape mais c'est manuelle.

## Mise en forme des données en entrée de la simulation

Cette partie permet de préparer les données pour qu'elles correspondent au format attendu par Openfisca. En effet POTE contient une ligne par foyer fiscal avec dans cette ligne à la fois des informations à l'échelle du foyer fiscal et des informations pour chaque individu qui le composent. En entrée d'Openfisca nous devons fournir deux tables :

- Une table individu avec une ligne individu, avec pour chaque ligne les informations spécifiques à chaque individu (date de naissance, revenus individuels)

- Une table foyer fiscal

Exemple :

Pote brut : 

| identifiant_foyer | salaire_declarant | salaire_conjoint  | salaire_pac     | revenus_capital_foyer | revenus_foncier_foyer |
| :---------------: | :---------------: | :---------------: | :-------------: | :-------------------: | :-------------------: |
| 1                 | 30000             | 25000             | NaN             | 6000                  | 0                     |
| 2                 | 10000             | NaN               | NaN             | 0                     | 0                     |
| 3                 | 50000             | 0                 | 5000            | 0                     | 4000                  |


Table individus finale :

| identifiant_foyer   | individu  | salaire  |
| :----------------:  | :-------: | :------: |
| 1                   | declarant | 30000 |
| 1                   | conjoint  | 25000 |
| 2                   | declarant | 10000 |
| 3                   | declarant | 50000 |
| 3                   | conjoint  | 0 |
| 3                   | pac       | 5000 |

Table foyers fiscaux finale :

| identifiant_foyer   | revenus_capital_foyer  | revenus_foncier_foyer  |
| :----------------:  | :------------------:   | :--------------------: |
| 1                   | 6000                   | 0                      |
| 2                   | 0                      | 0                      |
| 3                   | 0                      | 4000                   |


### step_00_variables_pote

Cette étape préliminaire définit l'ensemble des variables de POTE qui vont nous servir comme variables d'entrée de la simulation. Pour les définir on compare les variables présentes dans POTE avec les cases fiscales (CERFA) renseignées dans les variables Openfisca France pour l'année des données. En effet dans OpenFisca France il y a une correspondance entre les variables de revenus et les cases de la déclaration fiscale à laquelle elle correspond. Par exemple la variable `salaire_imposable` corresponds aux cases fiscales `1AJ`, `1BJ`, `1CJ`, `1DJ`, `1EJ` (respectivement traitement et salaire du déclarant 1 et 2, personne à charge 1, 2 et 3) de la déclaration fiscale. 

On obtient un dictionnaire de variables à l'échelle individuelle et un dictionnaire de variables à l'échelle du foyer fiscal. Chaque dictionnaire fait la correspondance entre le nom des variables Openfisca et la ou les cases cerfa correspondantes.

Exemple : 

    variables_individu = {
                          'salaire_imposable':['1AJ', '1BJ', '1CJ', '1DJ', '1EJ'],
                          'chomage_imposable':['1AP', '1BP', '1CP', '1DP', '1EP']
                          }
    
    variables_foyer = {
                       'f3vg':['3VG'], #'Plus-value imposable'
                       'f4ba':['4BA] # Revenus fonciers imposables
                        } 

A noter que dans OpenFisca France les variables de revenus définies à l'échelle du foyer fiscal porte preque toute le nom de la case fiscale correspondante dans les déclarations de revenus.

Cette étape résulte du fait que dans OpenFisca certaines cases fiscales qui ne concernent pas beaucoup de foyer fiscaux ne sont pas codées, nous ne les prenons donc pas dans POTE pour la simulation.

### step_01_create_individus

Cette étape vise à sélectionner les variables définies à l'échelle de l'individu (comme les traitements et salaires) afin de créer une table individus avec une ligne par individus.


Pour rappel dans POTE les revenus individuels sont tous situés sur la même lignes avec une colonne différentes par personnes du foyer fiscal :


| identifiant_foyer   | salaire_declarant | salaire_conjoint  | salaire_pac     |
| :-----------------: | :---------------: | :---------------: | :-------------: |
| 1                   | 30000             | 25000             | NaN             |
| 2                   | 10000             | NaN               | NaN             |
| 3                   | 50000             | 0                 | 5000            |

L'objectif est de transformer cette table pour obtenir une table ayant le format adapté à OpenFisca : 

| identifiant_foyer   | individu  | salaire  |
| :----------------:  | :-------: | :------: |
| 1                   | declarant | 30000 |
| 1                   | conjoint  | 25000 |
| 2                   | declarant | 10000 |
| 3                   | declarant | 50000 |
| 3                   | conjoint  | 0 |
| 3                   | pac       | 5000 |


Pour cela nous commençons par déduire le nombre d'individus dans chaque foyer en fonction des dates de naissances des déclarants et personnes à charge renseignées. Ensuite nous attribuons à chaque individus identifié ses revenus. Pour les personnes à charge, nous n'avons pas de correspondance stricte entre les dates de naissances et les revenus. Nous considérons l'ordre des personnes à charge en fonction de leur date de naissance : nous considérons que la plus âgée est la personne à charge 1.

Nous obtenons ainsi I individus à partir des F foyers fiscaux de POTE 2022. Voir la section "Champ de POTE" (mettre lien ? ) pour plus de détails sur qui sont ces individus. Il faut avoir en tête que certaines personnes peuvent apparaître deux fois dans POTE si la personne à fait deux déclarations dans l'année, par exemple en cas de décès d'un conjoint.

### step_02 : création de la table foyer fiscal

A l'inverse des variables à l'échelle de l'individu, les revenus définis pour l'ensemble du foyer fiscal sont directement dans le bon format de colonne. Cependant la complexité de ces variables résident dans leur nombre très important, plus de 800 variables, notamment du fait des cases de crédits d'impôt. Le volume de données est ainsi trop important pour être utilisé tel quel dans une simulation. Pour résoudre cela nous diminuons le nombre de colonnes en réalisant des calculs intermédiaires. Certaines variables intermédiaires d'openfisca sont ainsi la somme de cases fiscales, regroupées ensemble pour la suite du calcul. De ce fait en calculant ces variables et en les prenant comme variable d'entrée nous réduisons le nombre de colonnes des tables d'entrée sans pour autant diminuer la capacité de faire des simulations de réformes.

Par exemple la variable `domlog` qui regroupe les réductions d'impôt au titre des investissements outre-mer réalisés par des personnes physiques fait la somme de 64 cases fiscales de crédits d'impôts pour l'année 2021. En calculant cette variable et en l'utilisant en entrée de la simulation finale nous gagnons ainsi 63 colonnes.


#### step_02_a : sélection des variables à précalculer

On sélectionne les variables à précalculer suivant les conditions suivantes :

- La variable ne fait appel qu'à des variables d'input (sans formule) de cases fiscales.
- Les cases fiscales qui sont appelées dans cette variable ne sont utilisées que dans cette variable dans le calcul de l'impôt sur le revenu. En effet l'objectif est de supprimer ces variables de cases fiscales des variables d'entrée. Il faut donc s'assurer qu'elles ne soient pas utilisées à d'autres endroits.

Cette sélection de variable est faite par la fonction `liens_variables` dans le fichier `analyse_variables.py`. On réalise une simulation de calcul de l'impôt sur le revenu d'un cas type simple afin de récupérer la trace de la simulation. Cela nous permet de voir toutes les variables calculées, pour quelles périodes et combien de fois une variable est appelée. 19 variables sont ainsi éligibles au précalcul et font ainsi gagner XXXX colonnes d'entrée.

#### step_02_b : précalcul des variables sélectionnées avec openfisca 

Pour chacune des 19 variables sélectionnées dans l'étape précédente on réalise une simulation openfisca pour calculer la valeur de cette variable à partir des cases fiscales en entrée. Puis la variable calculée est enregistrer dans un fichier temporaire `.parquet`.

#### step_02_c : préparation de la table d'input de la simulation finale

Cette dernière étape consiste à construire une table foyer_fiscal unique contenant les cases fiscales définies à l'échelle du foyer fiscal à l'exeption des variables de cases fiscales utilisées dans l'étape précédente, qui sont remplacées par les variables pré-calculées.
Même en réalisant une étape de calcul, nous avons encore trop de variables d'entrée par rapport à la capacité de notre instance au CASD. Pour pallier cela nous sélectionnons uniquement les variables ayant plus de 10 000 valeurs non nulles, c'est à dire qu'au moins 10 000 foyers fiscaux ont rempli cette case. Les plus petites cases fiscales en terme de nombre de foyers concernées ne sont donc pour l'instant pas utilisées dans la simulation.
Cette sélection semble avoir un effet marginal puisque nous obtenons une erreur sur la masse totale de l'impôt sur le revenu de moins de 2%.