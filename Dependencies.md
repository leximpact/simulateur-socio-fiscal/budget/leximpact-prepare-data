```mermaid
stateDiagram-v2
    [*] --> leximpact_prepare_data
    leximpact_prepare_data --> leximpact_survey_scenario
    leximpact_prepare_data --> leximpact_common_python_libraries
    leximpact_survey_scenario --> openfisca_france_data
    openfisca_france_data --> openfisca_france :master
    leximpact_survey_scenario --> openfisca_france_reforms
    openfisca_france_reforms --> openfisca_france :wip-leximpact
```
