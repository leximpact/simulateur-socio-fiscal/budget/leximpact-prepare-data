.ONESHELL:
SHELL := /bin/bash
SRC = $(wildcard notebooks/*.ipynb)

all: leximpact_prepare_data docs


leximpact_prepare_data: $(SRC)
	-$(MAKE) lib
	touch leximpact_prepare_data

install:
	poetry self add poetry-bumpversion
	poetry install
	poetry run python -m ipykernel install --name leximpact-prepare-data-kernel --user
	poetry run pre-commit install

precommit:
	poetry run nbdev_clean
	# The - prevent stopping on error
	-poetry run pre-commit run --all-files
	-echo "XXXXXXXX Last check XXXXXXXXXXX"
	-poetry run pre-commit run --all-files

docs:
	$(MAKE) doc

doc:
	poetry run nbdev_docs
	curl https://cdn.plot.ly/plotly-2.14.0.min.js -o _docs/memos/plotly.js

test:
	poetry run nbdev_test_nbs
	poetry run pytest

test-nb:
	# poetry run papermill --execution-timeout 300 tests/calib_and_copule.ipynb ./papermill/calib_and_copule.ipynb

bump_patch:
	poetry version patch
	$(MAKE) precommit

bump_minor:
	poetry version minor
	$(MAKE) precommit

bump_major:
	poetry version major
	$(MAKE) precommit

conda_release:
	poetry run fastrelease_conda_package

pypi: dist
	poetry run twine upload --repository pypi dist/*

dist: clean
	poetry run python setup.py sdist bdist_wheel

clean:
	rm -rf dist

clear-nb:
	# You need nbconvert >= 6 for --clear-output option
	find ./notebooks/ -name '*.ipynb' ! -path "*/memos/*" ! -iname '*_opendata.ipynb' ! -iname 'index.ipynb' !  -iname 'calib_and_copule.ipynb' ! -path "*/.ipynb_checkpoints/*" -exec jupyter nbconvert --clear-output --inplace  \{\} \;
