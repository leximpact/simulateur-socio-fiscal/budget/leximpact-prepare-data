# from leximpact_prepare_data.scenario_tools.comparison import LeximpactErfsComparator
from leximpact_prepare_data.scenario_tools.comparison import LeximpactErfsComparator
from leximpact_common_python_libraries.config import Configuration
import os

default_target_variables = [
    "revenu_assimile_salaire",
    "chomage_imposable",
    # "salaire_imposable_large",
    "revenu_assimile_pension",
    "retraite_imposable",
    "pensions_invalidite",
    "pensions_alimentaires_percues",
    "revenu_categoriel_non_salarial",
    "rpns_imposables",
    "revenus_individuels",
    "revenu_categoriel_foncier",
    "rente_viagere_titre_onereux_net",
    "revenus_capitaux_prelevement_forfaitaire_unique_ir",
    "revenu_categoriel_capital",
    "revenus_capitaux_prelevement_bareme",
    "plus_values_prelevement_forfaitaire_unique_ir",
    "revenu_categoriel_plus_values",
    "taxation_plus_values_hors_bareme",
    "rfr",
    "rfr_par_part",
    "rni",
    "charges_deduc",
    "reductions",
    "credits_impot",
    "prlire",
    "ir_brut",
    "irpp_economique",
    "prelevement_forfaitaire_unique_ir",
    "contribution_exceptionnelle_hauts_revenus",
]


def test_aggregates(
    annee_de_calcul=2022,
    annee_donnees=2022,
    target_variables=default_target_variables,
):

    config = Configuration(project_folder="leximpact-prepare-data")
    config_files_directory = os.path.join(
        config.project_module_path, ".config", "openfisca-survey-manager"
    )

    comparator = LeximpactErfsComparator(
        period=annee_de_calcul,
        annee_donnees=annee_donnees,
        copules_comparaison=False,
        config_files_directory=config_files_directory,
    )
    comparator.compare(
        browse=False,
        load=False,
        verbose=False,
        debug=False,
        target_variables=target_variables,
        period=None,
        rebuild=True,
        summary=False,
    )
