# from leximpact_prepare_data.scenario_tools.comparison import LeximpactErfsComparator
from leximpact_prepare_data.pipeline_survey_scenario import PipelineErfsSurveyScenario
from openfisca_france_data.aggregates import FranceAggregates
from leximpact_common_python_libraries.config import Configuration
import os

default_aggregate_variables = [
    "salaire_imposable",
    "chomage_imposable",
    "retraite_imposable",
    "rpns_imposables",
    "revenu_categoriel_foncier",
    "revenus_capital",
    "plus_values_base_large",
    "irpp_economique",
    "af",
    "cf",
    "paje_base",
    "paje_naissance",
    "paje_prepare",
    "asf",
    "paje_cmg",
    "ars",
    "aides_logement",
    "apl",
    "aspa",
    "aah",
    "rsa",
    "ppa",
    "csg_salaire",
    "csg_non_salarie",
    "csg_chomage",
    "csg_retraite",
    "csg_remplacement",
]


def test_aggregates(
    annee_de_calcul=2022,
    annee_donnees=2022,
    source_cible="france_entiere",
    aggregate_variables=default_aggregate_variables,
):

    config = Configuration(project_folder="leximpact-prepare-data")

    output_path = config.get("TESTS_OUTPUT_PATH")
    config_files_directory = os.path.join(
        config.project_module_path, ".config", "openfisca-survey-manager"
    )
    leximpact_survey_scenario = PipelineErfsSurveyScenario(
        annee_donnees=annee_donnees,
        period=annee_de_calcul,
        config_files_directory=config_files_directory,
        collection="leximpact",
        survey_name=f"leximpact_{annee_donnees}",
    )

    aggregates = FranceAggregates(
        survey_scenario=leximpact_survey_scenario, target_source=source_cible
    )

    aggregates.year = annee_de_calcul

    aggregates.aggregate_variables = aggregate_variables

    aggregates.to_csv(path=output_path)
