"""Script to check version in pyproject.toml and compare it to versions in PyPi
JSON API.

If the version exist, exit with 1 to break CI.

Sample call:
python3 deploy/check_version.py -p leximpact-socio-fisca-simu-etat
"""

import argparse

import requests
import toml


def get_local_version():
    """
    Read the version in pyproject.toml
    :return: The version
    """
    conf = toml.load("pyproject.toml")
    return conf["tool"]["poetry"]["version"]


def get_versions_from_pypi(package_name: str = "") -> dict:
    """Get package versions from PyPi JSON API.

    ::package_name:: Name of package to get infos from.
    ::return:: A list of versions.
    """
    if package_name == "":
        raise ValueError("Package name not provided.")
    url = f"https://pypi.org/pypi/{package_name}/json"
    print("Calling", url)
    resp = requests.get(url)
    if resp.status_code == 404:
        raise ValueError(
            f"ERROR Package '{package_name}' not found when calling PyPI ({url}) : {resp}"
        )
    if resp.status_code != 200:
        raise Exception(f"ERROR calling PyPI ({url}) : {resp}")
    resp = resp.json()
    versions = []
    for v in resp["releases"]:
        versions.append(v.lower().strip())
    return versions


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-p",
        "--package",
        type=str,
        default="",
        required=True,
        help="The name of the package",
    )
    parser.add_argument(
        "-o",
        "--onlyprintversion",
        action="store_true",
        default=False,
        help="Only print the local version of the package.",
    )
    args = parser.parse_args()
    versions = get_versions_from_pypi(args.package)
    local_version = get_local_version()
    if args.onlyprintversion:
        print(local_version)
    elif local_version.lower().strip() in versions:
        print(f"Version {local_version} already exist on PyPi !")
        print("Please run 'poetry version patch && make precommit' and commit changes.")
        exit(1)
    else:
        print(f"OK, local version is {local_version} and PyPi version are {versions}")
