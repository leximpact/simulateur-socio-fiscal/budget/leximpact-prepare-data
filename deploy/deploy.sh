#!/bin/bash
##########
# Ce script prépare le serveur à recevoir les docs.
# Plus utilisé depuis que les docs sont en HTML
##########
CI_COMMIT_REF_NAME=$1
CI_COMMIT_SHA=$2

echo "Starting deploy-doc-step-1.sh as $USER in $PWD with param:"
echo "CI_COMMIT_REF_NAME=$CI_COMMIT_REF_NAME"
echo "CI_COMMIT_SHA=$CI_COMMIT_SHA"
echo "SERVICE_NAME=$SERVICE_NAME"
git fetch --all
git reset --hard origin/$CI_COMMIT_REF_NAME
git checkout -B $CI_COMMIT_REF_NAME $CI_COMMIT_SHA
git pull origin $CI_COMMIT_REF_NAME
