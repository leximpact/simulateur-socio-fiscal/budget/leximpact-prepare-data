# Utilisation de Docker pour LexImpact Prepare Data

## Préparer l'environnement

Il faut récupérer le projet Git :
```bash
git clone git@git.leximpact.dev:leximpact/leximpact-prepare-data.git
cd leximpact-prepare-data
git checkout 2-open-fisca-survey-scenario-pour-piloter-les-simulations-de-reforme
# Il faut cloner openfisca-france-data à l'intérieur de leximpact-prepare-data
# pour pouvoir le modifier dans l'image Docker
git clone git@git.leximpact.dev:benjello/openfisca-france-data.git
cd openfisca-france-data
git checkout leximpact-prepare-data
cd ..
```


## Construction de l'image


Depuis la racine du projet `leximpact-prepare-data`:
```bash
docker build -t leximpact/prepare-data:0.0.1 -f deploy/Dockerfile-Debian11-Python39 .
## Construction de l'image

Il faut récupérer le projet Git :
```bash
git clone git@git.leximpact.dev:leximpact/leximpact-prepare-data.git
cd leximpact-prepare-data
git checkout 2-open-fisca-survey-scenario-pour-piloter-les-simulations-de-reforme
# Il faut cloner openfisca-france-data à l'intérieur de leximpact-prepare-data
# pour pouvoir le modifier dans l'image Docker
git clone git@git.leximpact.dev:benjello/openfisca-france-data.git
cd openfisca-france-data
git checkout leximpact-prepare-data
cd ..
```


## Construction de l'image


Depuis la racine du projet `leximpact-prepare-data`:
```bash
docker build -t leximpact-prepare-data -f deploy/Dockerfile-Debian11-Python39 .
```

## Copier les fichiers de données

```bash
mkdir -p data-in/casd_extract/pote/
mkdir -p data-out/leximpact/erfs-fpr/
mkdir -p data-out/plots/
cp /mnt/data-out/leximpact/agregats.yml data-out/leximpact/
cp /mnt/data-in/casd_extract/pote/agregats_des_variables_csg-POTE_2019.csv data-in/casd_extract/pote/
cp /mnt/data-out/leximpact/erfs-fpr/erfs_flat_2018.h5 data-out/leximpact/erfs-fpr/
cp /mnt/data-in/casd_extract/pote/CalibPote-2018-revkire.json data-in/casd_extract/pote/
cp /mnt/data-in/casd_extract/pote/CalibPote-2019-revkire.json data-in/casd_extract/pote/
cp /mnt/data-out/plots/Calibration_de_rfr_ERFS_2018_POTE_2018_avant_calibration.png data-out/plots/
```

## Lancer le script de test de pipeline

```bash
docker run -v $PWD/data-in/:/mnt/data-in/ -v $PWD/data-out/:/mnt/data-out/ leximpact/prepare-data:0.0.1 poetry run python3 ./survey_scenario/survey_scenario.py
```

## Instancier l'image et obtenir un shell dans le conteneur

`docker run -it -v $PWD/data-in/:/mnt/data-in/ -v $PWD/data-out/:/mnt/data-out/ leximpact/prepare-data:0.0.1 bash`

## Publication de l'image

```bash
docker push leximpact/prepare-data:0.0.1
```
